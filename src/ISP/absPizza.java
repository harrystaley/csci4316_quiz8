package ISP;

import static ISP.Main.stdIn;

enum sizes {
    LG, MED, SM, XL, COWABUNGA;
}

enum crusts {
    CHICAGO, HANDTOSSED, NEWYORK, PAELEO, SHREDDER;
}

public abstract class absPizza {

    protected static String[] recipes = {"PEPPERONI", "CARNIVORE", "VEGGIE", "NEAPOLITAN", "STROMBOLI", "CALZONE"};

    // GETTERS

    public sizes getSize() {
        return size;
    }

    public static String[] getRecipes() {
        return recipes;
    }

    public String getRecipe() {
        return recipe;
    }

    public Enum<crusts> getCrust() {
        return crust;
    }

    // SETTERS

    public void setSize(sizes size) {
        this.size = size;
    }

    public static void setRecipes(String[] recipes) {
        absPizza.recipes = recipes;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public void setCrust(Enum<crusts> crust) {
        this.crust = crust;
    }

    protected sizes size;

    // ABSTRACT METHODS
    abstract void bake();

    protected String recipe;
    protected Enum<crusts> crust;

    public static int recipeProm() throws IllegalStateException {
        if (absPizza.recipes.length == 0) throw new IllegalStateException("Null Menu");
        while (true) {
            System.out.println("WELCOME TO NINJA TURTLE PIZZA");
            System.out.println("Where we serve pizza on the half shell...\n");
            System.out.println("Select one of our famous specialties:");
            for (int i = 0; i < absPizza.recipes.length; i++) {
                System.out.println("\t" + (i + 1) + ". " + absPizza.recipes[i]);
            }// end for
            System.out.println("\"c\" Checkout");
            if (stdIn.hasNextInt()) {
                int option = Main.stdIn.nextInt();
                if (option <= absPizza.recipes.length && option > 0) {
                    System.out.println("You selected " + absPizza.recipes[option - 1]);
                    return option - 1;
                }// end if
            } else if (stdIn.next().equals("c")){
                return -1;
            }// end if
            System.out.println("Try again dude!");
        }// end while
    }// end class

    void sizeProm() {
        boolean sizeError = true;
        while (sizeError) {

            System.out.println("Select size:\n\t1. Small\n\t2. Medium\n\t3. Large\n\t4. Extra Large\n\t5. Turtle Size");
            System.out.println("\"l\"Leave without finishing your order.");

            if (stdIn.hasNextInt()) {
                int option = stdIn.nextInt();
                switch (option) {
                    case 1:
                        this.size = sizes.SM;
                        System.out.println("Small");
                        sizeError = false;
                        break;
                    case 2:
                        this.size = sizes.MED;
                        System.out.println("Medium");
                        sizeError = false;
                        break;
                    case 3:
                        this.size = sizes.LG;
                        System.out.println("Large");
                        sizeError = false;
                        break;
                    case 4:
                        this.size = sizes.XL;
                        System.out.println("Extra Large");
                        sizeError = false;
                        break;
                    case 5:
                        this.size = sizes.COWABUNGA;
                        System.out.println("Turtle Size");
                        sizeError = false;
                        break;
                    default:
                        continue;
                }// end switch
                break;
            } else {
                System.out.println("Sorry dude we don't have that size.");
                sizeError = true;
            }// end if
            if (stdIn.toString().equals("l")){
                sizeError = false;
                return;
            }// end if
        }// end while
    }// end method sizeProm

    void crustProm() {
        boolean crustError = true;
        while (crustError) {
            System.out.println("Select crust:\n\t1. New York\n\t2. Hand Tossed\n\t3. Chicago\n\t4. Paeleo\n\t5. Cheese Shreddn Shredder");
            System.out.println("\"l\"Leave before finishing your order.");

            if (stdIn.hasNextInt()) {
                switch (stdIn.nextInt()) {
                    case 1:
                        this.crust = crusts.NEWYORK;
                        System.out.println("New York");
                        crustError = false;
                        break;
                    case 2:
                        this.crust = crusts.HANDTOSSED;
                        System.out.println("Hand Tossed");
                        crustError = false;
                        break;
                    case 3:
                        this.crust = crusts.CHICAGO;
                        System.out.println("Chicago");
                        crustError = false;
                        break;
                    case 4:
                        this.crust = crusts.PAELEO;
                        System.out.println("Paeleo");
                        crustError = false;
                        break;
                    case 5:
                        this.crust = crusts.SHREDDER;
                        System.out.println("Cheese Shreddin Shredder");
                        crustError = false;
                        break;
                    default:
                        continue;
                }// end switch
                break;
            } else {
                System.out.println("Try again Bra!");
                crustError = true;
            }// end if
            if (stdIn.toString().equals("l")){
                crustError = false;
                return;
            }// end if
        }// end while
    }// end crustProm
}// end class absPizza
