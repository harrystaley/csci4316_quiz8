package ISP;

public class Stromboli extends absStromboli {

    @Override
    void roll() {
        System.out.println("Your stromboli is being rolled including all of its' ingredients...");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }// end try catch
        System.out.println("The stromboli done please pick up your order.");
    }// end roll

    @Override
    void bake() {
        System.out.println("Your order is baking... \nThe order preparation is finished.");
        roll();
        System.out.println("Order summary:");
        System.out.println("\tRecipe: " + this.getRecipe());
        System.out.println("\tSize: " + this.getSize());
        System.out.println("\tCrust: " + this.getCrust());
    }// end bake
}// end class Stromboli
