package ISP;

public class calzone extends absCalzone {

    @Override
    void fold() {
        System.out.println("Please wait, calzone is being folded with all of your toppings in it.");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }// end try catch
        System.out.println("Mikey has your fresh and hot calzone right ova dear!!.");
    }// end fold

    @Override
    void bake() {
        System.out.println("That onez done.");
        fold();
        System.out.println("Order summary:");
        System.out.println("\tRecipe: " + this.getRecipe());
        System.out.println("\tSize: " + this.getSize());
        System.out.println("\tCrust: " + this.getCrust());
    }// end bake
}// end class calzone
