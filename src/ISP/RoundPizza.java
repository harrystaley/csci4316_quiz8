package ISP;

import static ISP.Main.stdIn;

public class RoundPizza extends absRoundPizza {

    @Override
    public int getSlices() {
        return this.slices;
    }// end getSlices

    @Override
    public void setSlices(int slices) {
        this.slices = slices;
    }// end setSlices

    @Override
    void slice() {
        if (getSlices() < 0) {
            promptSlices();
        }// end if
        System.out.println("Donatello is slicing...\nYour pizza is sliced in " + this.getSlices() + " pieces. You can pick up your order.");
    }// end slice

    @Override
    void promptSlices() {
        while (true) {
            System.out.println("Select number of slices:");
            if (stdIn.hasNextInt()) {
                int selected = stdIn.nextInt();
                if (selected >= 0) {
                    this.setSlices(selected);
                    break;
                }// end if
            }// end if
            System.out.println("Please select at least 0 slices.");
        }// end while
    }// end promptSlices

    @Override
    void bake() {
        System.out.println("Your order is baking... \nThe order preparation is finished.");
        slice();
        System.out.println("Order summary: ");
        System.out.println("\tRecipe: " + this.getRecipe());
        System.out.println("\tSize: " + this.getSize());
        System.out.println("\tCrust: " + this.getCrust());
        System.out.println("\tSlices: " + this.getSlices());
    }// end bake

}// end class RoundPizza
