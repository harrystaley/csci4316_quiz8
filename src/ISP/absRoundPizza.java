package ISP;


public abstract class absRoundPizza extends absPizza {
    // this is germain only to the round pizza
    protected int slices;
    public abstract int getSlices();
    public abstract void setSlices(int slices);
    abstract void slice();
    abstract void promptSlices();
}// end class absRoundPizza
