package SANSISP;

import java.util.Objects;

import static ISP.Main.stdIn;

public class RoundPizza extends absRoundPizza {

    //--------------------
    //PIZZA
    public int getSlices() {
        return this.slices;
    }// end getSlices

    public void setSlices(int slices) {
        this.slices = slices;
    }// end setSlices

    @Override
    void slice() {
        if (getSlices() < 0) {
            promptSlices();
        }// end if
        System.out.println("Donatello is slicing...\nYour pizza is sliced in " + this.getSlices() + " pieces. You can pick up your order.");
    }// end slice

    @Override
    void promptSlices() {
        while (true) {
            System.out.println("Select number of slices:");
            if (stdIn.hasNextInt()) {
                int selected = stdIn.nextInt();
                if (selected >= 0) {
                    this.setSlices(selected);
                    break;
                }// end if
            }// end if
            System.out.println("Please select at least 0 slices.");
        }// end while
    }// end promptSlices

    @Override
    void bake() {
        System.out.println("Your order is baking... \nThe order preparation is finished.");
        slice();
        System.out.println("Order summary: ");
        System.out.println("\tRecipe: " + this.getRecipe());
        System.out.println("\tSize: " + this.getSize());
        System.out.println("\tCrust: " + this.getCrust());
        if (!Objects.equals(this.getRecipe(), "STROMBOLI") && !Objects.equals(this.getRecipe(), "CALZONE")) {
            System.out.println("\tSlices: " + this.getSlices());
        }// end if
    }// end bake

    //-----------------
    //CALZONE
    @Override
    void fold() {
        System.out.println("Please wait, calzone is being folded with all of your toppings in it.");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }// end try catch
        System.out.println("Mikey has your fresh and hot calzone right ova dear!!.");
    }// end fold

    //------------------
    //STROMBOLI
    @Override
    void roll() {
        System.out.println("Your stromboli is being rolled including all of its' ingredients...");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }// end try catch
        System.out.println("The stromboli done please pick up your order.");
    }// end roll

}// end class RoundPizza
