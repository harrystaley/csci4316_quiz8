package SANSISP;

/*
Developer: Harry Staley
*/

import java.util.Scanner;

public class Main {

    // import scanner to accept input from standard in.
    public static Scanner stdIn = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("START W/O ISP EXAMPLE\n\n");

        boolean moreItems = true;
        while (moreItems) {
            int pizzaRecipeIndex = absPizza.recipeProm();
            if (pizzaRecipeIndex == -1) {
                return;
            }// end if
            absPizza order;
            switch (pizzaRecipeIndex) {
                case 0:
                    order = new RoundPizza();
                    break;
                case 1:
                    order = new RoundPizza();
                    break;
                case 2:
                    order = new RoundPizza();
                    break;
                case 3:
                    order = new RoundPizza();
                    break;
                case 4:
                    order = new RoundPizza();
                    break;
                case 5:
                    order = new RoundPizza();
                    break;
                default:
                    throw new IllegalArgumentException("That aint on da menu son get somthin else before Raphael over there gets 'Sarcastic'.");
            }// end switch

            order.setRecipe(absPizza.getRecipes()[pizzaRecipeIndex]);

            order.sizeProm();
            if (order.getSize() == null) return;

            order.crustProm();
            if (order.getCrust() == null) return;

            order.bake();
            System.out.println("Whad else can I git ya? y/n");

            if (stdIn.next().equals("n")) {
                moreItems = false;
                stdIn.close();
            }// end if
        }// end while
    }// end method main
}// end class main