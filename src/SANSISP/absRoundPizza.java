package SANSISP;


public abstract class absRoundPizza extends absPizza {
    // this is germain only to the round pizza
    protected int slices;
    public abstract int getSlices();
    public abstract void setSlices(int slices);
    abstract void slice();
    abstract void promptSlices();

    // ABSTRACT MEHTODS
    // Germain only to stromboli.
    abstract void roll();
    abstract void fold();

}// end class ISP.absRoundPizza
